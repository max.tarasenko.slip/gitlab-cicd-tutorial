**Подключение GitLab Runner**

1) Заходим в настройки проекта (Settings > CI / CD > Runners)
2) Set up a specific Runner manually / Install GitLab Runner manually on GNU/Linux

Прописываем эти команды на нашем выделенном сервере:

```
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```


3) Register the Runner

`sudo gitlab-runner register`

Запустится регистрация, ссылка на сайт и токен доступа находится в Settings > CI / CD > Runners

* Enter your GitLab instance URL: Ссылка в настройках
* Enter the token you obtained to register the Runner: Токен доступа в настройках
* Enter a description for the Runner, you can change this later in GitLab’s UI: Даем название нашему ранеру
* Enter the tags associated with the Runner, you can change this later in GitLab’s UI: Прописываем теги, для указания бегунка в сценариях
* Enter the Runner executor: Выбираем исполнителя для нашего бегунка
* If you chose Docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in .gitlab-ci.yml: По дефолту лучше всего выставить alpine:latest


Установка и регистрация первого бегунка готова


**Создание и запуск конвейера Gitlab CI/CD**

Пример простой конфигурации для запуска Gitlab CI/CD

https://gitlab.com/max.tarasenko.slip/gitlab-cicd-tutorial/-/blob/master/.gitlab-ci.yml

* stages – прописываем этапы, которые у нас будут
* далее пишем название нашему этапу
* stage – пишем ему этап, который мы планировали
* script – прописываем команды, которые будет выполнять наш stage
* tags – пишем теги, для идентификации и использования нужного бегунка
* artifacts – список файлов и каталогов для прикрепления


Полный справочник по конфигурации конвейера GitLab CI / CD

https://gitlab.com/help/ci/yaml/README
